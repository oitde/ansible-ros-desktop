#!/bin/bash
#
# spawn a child process to do the actual install, 
# and do it in a way that will not terminate even if the parent goes away
#

nohup ./xfce-install.sh 2>&1 | tee -a /tmp/rapid_image_status.txt /tmp/xfce_install_log.txt & 
