# Ansible Robot Operating System (ROS) / Ubuntu Desktop

For use with VCM/RAPID - setup an Ubuntu Xfce desktop for remote access

Supports both x2go and FastX3 for remote access

See https://www.ros.org for application info
